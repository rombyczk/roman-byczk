package pelzacz;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JPanel;

@SuppressWarnings("serial")

public class RenderPanel extends JPanel
{

	public static final Color GREEN = new Color(1666073);

	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		
		robak snake = robak.snake;

		g.setColor(GREEN);
		
		g.fillRect(0, 0, 800, 700);

		g.setColor(Color.BLUE);

		for (Point point : snake.snakeParts)
		{
			g.fillRect(point.x * robak.SCALE, point.y * robak.SCALE, robak.SCALE, robak.SCALE);
		}
		
		g.fillRect(snake.head.x * robak.SCALE, snake.head.y * robak.SCALE, robak.SCALE, robak.SCALE);
		
		g.setColor(Color.RED);
		
		g.fillRect(snake.cherry.x * robak.SCALE, snake.cherry.y * robak.SCALE, robak.SCALE, robak.SCALE);
		g.setColor(Color.BLACK);
		g.fillRect(snake.wall.x * robak.SCALE, snake.wall.y * robak.SCALE, robak.SCALE, robak.SCALE);
		
		String string = "Score: " + snake.score + ", Length: " + snake.tailLength + ", Time: " + snake.time / 20;
		
		g.setColor(Color.white);
		
		g.drawString(string, (int) (getWidth() / 2 - string.length() * 2.5f), 10);

		string = "Game Over!";

		if (snake.over)
		{
			g.drawString(string, (int) (getWidth() / 2 - string.length() * 2.5f), (int) snake.dim.getHeight() / 4);
		}

		string = "Paused!";

		if (snake.paused && !snake.over)
		{
			g.drawString(string, (int) (getWidth() / 2 - string.length() * 2.5f), (int) snake.dim.getHeight() / 4);
		}
	}
}