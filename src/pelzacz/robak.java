package pelzacz;

import java.util.*;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JFrame;
import javax.swing.Timer;

public class robak implements ActionListener, KeyListener
{

	public static robak snake;

	private JFrame jframe;

	private RenderPanel renderPanel;

	private Timer timer = new Timer(50, this);

	public ArrayList<Point> snakeParts = new ArrayList<Point>();

	public static final int UP = 0, DOWN = 1, LEFT = 2, RIGHT = 3, SCALE = 10;

	public int ticks = 0, direction = DOWN, score, tailLength = 10;
	
	public float time;

	public Point head, cherry, wall;

	private Random random;

	public boolean over = false, paused;

	public Dimension dim;

	public robak()
	{
		dim = Toolkit.getDefaultToolkit().getScreenSize();
		jframe = new JFrame("Snake");
		jframe.setVisible(true);
		jframe.setAutoRequestFocus(true);
		jframe.setAlwaysOnTop(true);
		jframe.setSize(805, 700);
		jframe.setResizable(false);
		jframe.setLocation(dim.width / 2 - jframe.getWidth() / 2, dim.height / 2 - jframe.getHeight() / 2);
		jframe.add(renderPanel = new RenderPanel());
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.addKeyListener(this);
		startGame();
	}

	public void startGame()
	{
		over = false;
		paused = false;
		time = 0;
		score = 0;
		tailLength = 10;
		ticks = 0;
		direction = RIGHT;
		head = new Point(0, 0);
		random = new Random();
		snakeParts.clear();
		cherry = new Point(random.nextInt(79), random.nextInt(66));
		wall = new Point(random.nextInt(79), random.nextInt(66));
		timer.start();
	}

	@Override
	public void actionPerformed(ActionEvent arg0)
	{
		renderPanel.repaint();
		ticks++;

		if (ticks % 2 == 0 && head != null && !over && !paused)
		{
			time+=50/20;

			snakeParts.add(new Point(head.x, head.y));

			if (direction == UP)
			{
				if (head.y - 1 >= 0 && noTailAt(head.x, head.y - 1))
				{
					head = new Point(head.x, head.y - 1);
				}
				else
				{
					over = true;

				}
			}

			if (direction == DOWN)
			{
				if (head.y + 1 < 67 && noTailAt(head.x, head.y + 1))
				{
					head = new Point(head.x, head.y + 1);
				}
				else
				{
					over = true;
				}
			}

			if (direction == LEFT)
			{
				if (head.x - 1 >= 0 && noTailAt(head.x - 1, head.y))
				{
					head = new Point(head.x - 1, head.y);
				}
				else
				{
					over = true;
				}
			}

			if (direction == RIGHT)
			{
				if (head.x + 1 < 80 && noTailAt(head.x + 1, head.y))
				{
					head = new Point(head.x + 1, head.y);
				}
				else
				{
					over = true;
				}
			}
			if(head.equals(wall))
			{
				over=true;
			}
			

			if (snakeParts.size() > tailLength)
			{
				snakeParts.remove(0);

			}

			if (cherry != null)
			{
				if (head.equals(cherry))
				{
					score += 10;
					tailLength++;
					cherry.setLocation(random.nextInt(79), random.nextInt(66));
				}
			}
		}
	}

	public boolean noTailAt(int x, int y)
	{
		for (Point point : snakeParts)
		{
			if (point.equals(new Point(x, y)))
			{
				return false;
			}
		}
		return true;
	}
	static void  WhatNumber(int number) throws NumberException
	{
    if(number ==2)
    {
    	snake = new robak();
    }
    else if(number==1)
    {
    	wordCounter();
    }
    else
    	throw new NumberException("Sorry!! Wrong number");
  }
	public static void wordCounter()
	{
		System.out.println("Welcome to wordCounter");
		System.out.print("Type the text:");
		@SuppressWarnings("resource")
		Scanner input = new Scanner(System.in);
		String inputText = input.nextLine();
		String[] words = inputText.split("[ \n\t\r,.;{}();:?!}]");
		TreeMap<String, Integer> map= new TreeMap<String, Integer>();
		for(int i=0;i<words.length;i++)
		{
			String key = words[i].toLowerCase();
			if(words[i].length()>1)
			{
				if(map.get(key)==null)
				{
					map.put(key, 1);
				}
				else
				{
					int value = map.get(key).intValue();
					value++;
					map.put(key, value);
				}
			}
		}
		System.out.println(map);
		
	}
	@SuppressWarnings("resource")
	public static void main(String[] args) 
	{
		System.out.println("What would you like to do?");
		System.out.println("ex1 go to wordCounter? type 1");
		System.out.println("ex2 play pe�zacz? type 2");
		
		Scanner Number = new Scanner(System.in);
		int inputNumber= Number.nextInt();
		
		try{
		WhatNumber(inputNumber) ;
		}
		catch(NumberException e)
		{
		  e.printStackTrace();
		}
		
	}

	@Override
	public void keyPressed(KeyEvent e)
	{
		int i = e.getKeyCode();

		if ((i == KeyEvent.VK_A || i == KeyEvent.VK_LEFT) && direction != RIGHT)
		{
			direction = LEFT;
		}

		if ((i == KeyEvent.VK_D || i == KeyEvent.VK_RIGHT) && direction != LEFT)
		{
			direction = RIGHT;
		}

		if ((i == KeyEvent.VK_W || i == KeyEvent.VK_UP) && direction != DOWN)
		{
			direction = UP;
		}

		if ((i == KeyEvent.VK_S || i == KeyEvent.VK_DOWN) && direction != UP)
		{
			direction = DOWN;
		}

		if (i == KeyEvent.VK_SPACE)
		{
			if (over)
			{
				startGame();
			}
			else
			{
				paused = !paused;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e)
	{
	}

	@Override
	public void keyTyped(KeyEvent e)
	{
		
	}

}